# module API
#   module V1
#     class Default < Grape::API
#       resource :test do
#         params do
#           requires :name
#         end
#         get :response do
#           "Hello #{params[:name]}";
#         end
#       end
#     end
#   end
# end
module API
    class Base < Grape::API
      prefix 'api'
      format :json
      version 'v1'
      mount API::V1::Default

      add_swagger_documentation api_version: 'v1',
                                hide_documentation_path: true,
                                base_path: '/api'
    #
    end
end