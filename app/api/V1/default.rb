module API
  module V1
    class Default < Grape::API
      version 'v1'
      format 'json'
      resource :test do
        params do
          requires :name
        end
        get do
          "Hello #{params[:name]}"
        end
      end
    end
  end
end